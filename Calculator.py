import tkinter as tk
from tkinter import *


root=tk.Tk()
root.title("My simple calculator")
root.geometry("700x600")
root['background']='#856ff8'


a=IntVar()
b=IntVar()
def add():
    sum=a.get()+b.get()
    result.config(text="Result is "+str(sum))

def subtract():
    difference=a.get()-b.get()
    result.config(text="Result is "+str(difference))

def multiply():
    product=a.get()*b.get()
    result.config(text="Result is "+str(product))

def divide():
    quotient=a.get()/b.get()
    result.config(text="Result is "+str(quotient))


tk.Label(root,text="Enter First Number",bg="black",fg="white",font="Verdana 11 bold").grid(row=4,column=0,ipady=5)
tk.Label(root,text="Enter Second Number",bg="black",fg="white",font="Verdana 11 bold").grid(row=5,column=0,ipady=5)

e1=tk.Entry(root,textvariable=a,width=20,borderwidth=20,font="Verdana 8 bold").grid(row=4,column=1,padx=15,pady=30)
e2=tk.Entry(root,textvariable=b, width=20,borderwidth=20,font="Verdana 8 bold").grid(row=5,column=1,padx=15,pady=30)



btn=Button(root, text="ADD",bg="black",fg="white",font="Verdana 8 bold",width=8,borderwidth=15,command=add)
btn.grid(row=7,column=0,pady=10)
btn=Button(root, text="SUBTRACT",bg="black",fg="white",font="Verdana 8 bold",width=8,borderwidth=15,command=subtract)
btn.grid(row=7,column=1,pady=10,ipadx=6)
btn=Button(root, text="MULTIPLY",bg="black",fg="white",font="Verdana 8 bold",width=8,borderwidth=15,command=multiply)
btn.grid(row=7,column=2,padx=8,pady=10)
btn=Button(root, text="DIVIDE",bg="black",fg="white",font="Verdana 8 bold",width=8,borderwidth=15,command=divide)
btn.grid(row=7,column=3,padx=50,pady=10)

result=tk.Label(root,text=" Result ",bg="black",fg="yellow",font="Verdana 11 bold ")
result.grid(row=15,column=1,ipady=5,pady=10)

root.mainloop()